import { addDestroyableEventListener } from './utils.js'
export default (() => {

let template = `<div class="view">
       <input class="toggle" type="checkbox">
		   <label></label>
	       <button></button>
   </div>
   <input class="edit" value="Rule the web">    
`;


class TodoItem extends HTMLElement {
    static get observedAttributes() {
        return ['placeholder', 'autofocus'];
    }
    constructor() {
        super();
        //  this.attachShadow({mode: 'open'});
    }
    get label() {
        return this.querySelector('label');
    }
    get editField() {
        return this.querySelector('.edit');
    }
    get checkbox() {
        return this.querySelector('[type="checkbox"]');
    }
    get deleteButton() {
        return this.querySelector('button');
    }
    get value() {
        return this.getAttribute('value') || '';
    }
    set value(val) {
        this.setAttribute('value', val);
        this.label.textContent = val;
    }
    get todoId() {
        return this.getAttribute('todo-id') || '';
    }
    set todoId(val) {
        this.setAttribute('todoId', val);
    }
    get completed() {
        return this.hasAttribute('completed');
    }
    set completed(val) {
        val?this.setAttribute('completed','')&&(this.checkbox.checked=true):this.removeAttribute('completed')&&(this.checkbox.checked=false);
    }
    get view() {
        return this.querySelector('.view');
    }
    set editing(val){
       val? this.setAttribute('editing',''):this.removeAttribute('editing');
    }
    connectedCallback(){
        this.innerHTML = template;
        this._syncAttributes();
        this.checkboxListener = addDestroyableEventListener(this.checkbox, 'change', (e)=>{
            this.completed = e.target.checked;
            e.preventDefault();
            e.stopPropagation();
            this.dispatchEvent(new CustomEvent('change',{bubbles:true}));
        })
        this.deleteListener = addDestroyableEventListener(this.deleteButton, 'click', (e)=>{
            e.preventDefault();
            e.stopPropagation();
            this.dispatchEvent(new CustomEvent('delete',{bubbles:true}));
        })
        this.doubleClickListener = addDestroyableEventListener(this.view, 'dblclick', (e)=>{
            e.preventDefault();
            e.stopPropagation();
            this.editing=true;
            this.editField.focus();
        })
        this.blurListener = addDestroyableEventListener(this.editField, 'blur', (e)=>{
            e.preventDefault();
            e.stopPropagation();
            this.editing=false;
        })
        this.changeListener = addDestroyableEventListener(this.editField, 'change', (e)=>{
            this.value = e.target.value;
            e.preventDefault();
            e.stopPropagation();
            this.dispatchEvent(new CustomEvent('change',{bubbles:true}));

        })
        this.keypressListener = addDestroyableEventListener(this.editField, 'keydown', (e)=>{
            e.code === "Enter" && e.target.blur();
        })
        
    }
    attributeChangedCallback(name, oldValue, newValue){
        this._syncAttributes();
    }
    disconnectedCallback() {
        this.checkboxListener && this.checkboxListener.destroy() && delete this.checkboxListener;
        this.deleteListener && this.deleteListener.destroy() && delete this.deleteListener;
    }
    _syncAttributes() {
        this.label.textContent = this.value;
        this.checkbox.checked=this.completed;
        this.editField.value= this.value;
    }
}
    customElements.define('todo-item', TodoItem);
    return TodoItem;
})()