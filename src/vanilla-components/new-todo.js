import { addDestroyableEventListener } from './utils.js'

export default (() => {

// polyfill custom event


class NewTodo extends HTMLElement {
    
    static get observedAttributes() {
        return ['placeholder', 'autofocus'];
    }
    constructor(){
        super();
    }
    /**
     * Placeholder text to use
     * @type {String}
     */
    get placeholder() {
        return this.getAttribute('placeholder') || ''
    }
    get autofocus() {
        return this.hasAttribute('autofocus') || ''
    }
    set placeholder(val) {
        this.setAttribute('placeholder', val);
    }
    get input() {
        return this.querySelector('input');
    }
    get value() {
        return this.input.value;
    }
    set value(val) {
        this.input.value = val;
    }
    connectedCallback(){
        this.innerHTML='<input>';
        this._updateChildAttributes();
        this.keypressListener = addDestroyableEventListener(this.input, 'keydown', (e)=>{
            if(e.code === "Enter"){
                e.target.value && 
                e.target.value.trim().length>0 && 
                this.dispatchEvent(new CustomEvent('newitem',{bubbles:true,detail:{value:e.target.value}}));
                e.preventDefault();
                e.stopPropagation();
                e.target.value='';
            }
        });
    }
    attributeChangedCallback(name, oldValue, newValue){
        this._updateChildAttributes();
    }
    _updateChildAttributes(){
       this.input && (this.input.placeholder = this.placeholder);
       this.input && (this.autofocus? this.input.setAttribute('autofocus',''):this.input.removeAttribute('autofocus'));
    }
    disconnectedCallback() {
        this.input.removeEventListener(this.handler);
        delete this.handler;
    }
}
    customElements.define('new-todo', NewTodo);
    return NewTodo;
})()