import { addDestroyableEventListener } from './utils.js'
export default (() => {
    class ToggleAll extends HTMLElement {
        
        static get observedAttributes() {
            return ['checked'];
        }
        constructor(){
            super();
        }
        
        get checked() {
            return this.hasAttribute('checked');
        }
        set checked(val) {
            val?this.setAttribute('checked',''):this.removeAttribute('checked');
        }
        
        connectedCallback(){
            this.clickHandler = addDestroyableEventListener(this, 'click', e=>{
                this.checked = !this.checked;
                e.preventDefault();
                e.stopPropagation();
                this.dispatchEvent(new CustomEvent('change',{bubbles:true}));
            });
        }
        
        attributeChangedCallback(name, oldValue, newValue){
        }
        disconnectedCallback() {
            this.clickHandler && this.clickHandler.destroy() && delete this.clickHandler;
        }
    }
        customElements.define('toggle-all', ToggleAll);
        return ToggleAll;
    })()