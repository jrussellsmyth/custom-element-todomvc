import { Component, Input, ViewEncapsulation, HostBinding, ElementRef } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: [/*'./todo-item.component.css'*/],
    encapsulation: ViewEncapsulation.None

})
export class TodoItemComponent  {

   @Input('todo-id') todoId;
   // complete is simply about binding to the complete attribute of the host. 
   // use isCompleted to get a boolean completed state
   @HostBinding('attr.completed') @Input() completed;
   @HostBinding('attr.editing') @Input() editing;
   @HostBinding('attr.value') @Input() value;
   
   el:ElementRef=null;
   

    constructor (private elRef:ElementRef) {
        this.el = elRef;
    }
   
   
   startEdit ( ) {
       this.editing = "";
   }
   stopEdit ( ) {
       this.editing = null;

       this.el.nativeElement.dispatchEvent(new Event('change',{bubbles:true}));
       
   }
   fireChange(e) { 

   }
  toggleComplete( ) {
    this.completed = (this.completed===undefined || this.completed === null || this.completed === "false") ?"completed":null;
     //this.el.nativeElement.setAttribute('completed',this.isCompleted()?'completed':'false')
      this.el.nativeElement.dispatchEvent(new Event('change',{bubbles:true}));

  }
  deleteTodo( ) {
      this.el.nativeElement.dispatchEvent(new CustomEvent('delete',{bubbles:true}));

  }
  isCompleted() {
      return this.completed != null && this.completed != undefined && this.completed != 'false';
  }
   isEditing() {
       return this.editing != null && this.editing != undefined;
       console.log("editing = "+this.editing);
   }

}
