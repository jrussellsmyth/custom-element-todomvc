import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { TodoCountComponent } from './todo-count/todo-count.component';
import { TodoItemComponent } from './todo-item/todo-item.component';
import { ElementZoneStrategyFactory } from 'elements-zone-strategy';


@NgModule({
  declarations: [
    TodoCountComponent,
    TodoItemComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  entryComponents: [TodoCountComponent, TodoItemComponent]
})
export class AppModule { 
  constructor(private injector: Injector) {
    const todocount = createCustomElement(TodoCountComponent, {injector, strategyFactory:new ElementZoneStrategyFactory(TodoCountComponent, injector)});
    customElements.define('todo-count', todocount)

    const todoitem = createCustomElement(TodoItemComponent, {injector, strategyFactory:new ElementZoneStrategyFactory(TodoItemComponent, injector)});
    customElements.define('todo-item', todoitem)
  }
  
  ngDoBootstrap() {}
}
